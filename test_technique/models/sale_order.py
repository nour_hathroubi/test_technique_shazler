from odoo import api, models, fields, _
import logging
from odoo.exceptions import ValidationError


class Alert(models.TransientModel):
    _name = 'alert.message'

    note = fields.Text()


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.onchange('product_id')
    def onchange_product_id(self):
        occurence = 0
        for order in self.order_id.order_line:
            if self.product_id.id == order.product_id.id:
                occurence = occurence + 1
        if occurence > 2:
            message = 'vous avez ajouté l’article ' + str(self.product_id.name) + ' en double'
            query = 'delete from alert_message'
            self.env.cr.execute(query)
            value = self.env['alert.message'].create({'note': message})
            attachment_view = self.env.ref('test_technique.view_alert_wizard')
            return {
                'type': 'ir.actions.act_window',
                'name': 'Warning',
                'view_mode': 'form',
                'res_model': 'alert.message',
                'view_id': attachment_view.id,
                'context': {'active_id': self.id},
                'target': 'new',
                'res_id': value.id
            }
